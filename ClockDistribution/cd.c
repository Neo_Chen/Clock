#include <time.h>
#include <util/delay.h>
#include <avr/io.h>
#include <avr/interrupt.h>

#define DIV 1250

volatile unsigned int tick=0;
volatile uint8_t toggle=0;

int main(void)
{
	DDRD &= ~(1 << DDD2);     // Clear the PD2 pin
	// PD2 (INT0 pin) is now an input
	DDRB = 0xFF;

	PORTD |= (1 << PORTD2);    // turn On the Pull-up
	// PD0 is now an input with pull-up enabled


	MCUCR |= (1 << ISC00) | (1 << ISC01);    // set INT0 to trigger on rising edge
	GICR |= (1 << INT0);      // Turns on INT0

	sei();                    // turn on interrupts

	while(1)
	{
	}
}



ISR (INT0_vect)
{
	/* interrupt code here */
	tick++;

	if(tick >= (DIV / 2))
	{
		PORTB = toggle;
		toggle ^= 0xFF;
		tick = 0;
	}
}
