#!/bin/sh

PARTS=(AMTransmission ClockDistribution Frontend MainControl)
BASEDIR="$PWD"

rm -fv "$BASEDIR"/*.hex

for i in "${PARTS[@]}"; do
	(
		cd "$i"
		[ -f Makefile ] && make "$@"
		[ "${1}" != clean ] && cp -lv *.hex "$BASEDIR"
	)
done
