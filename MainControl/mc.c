/* UART */
#define USART_BAUDRATE 9600
#define BAUD_PRESCALE (((F_CPU / (USART_BAUDRATE * 16UL))) - 1)

#include <time.h>
#include <stdio.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "config.h"

volatile time_t epoch=0;
volatile int tick=1;
char buf=0;

struct tm *timeinfo;
char timestring[64];

static int uart_putchar(char c, FILE *stream);
static int recv(FILE *stream);
static FILE mystdout = FDEV_SETUP_STREAM(uart_putchar, recv,_FDEV_SETUP_RW);

static int uart_putchar(char c, FILE *stream)
{
	if (c == '\n')
	uart_putchar('\r', stream);
	loop_until_bit_is_set(UCSR0A, UDRE0);
	UDR0 = c;
	return 0;
}

static int recv(FILE *stream)
{
	while ((UCSR0A & (1 << RXC0)) == 0) {};
	return UDR0;
}

int main (void)
{
	int i=0;

	DDRD &= ~(1<<DDD2); // Input
	PORTD |= (1<<PORTD2); // Pull-up
	EICRA |= (1<<ISC00) | (1<<ISC01);
	EIMSK |= (1<<INT0);

	sei();

	UCSR0B |= (1 << RXEN0) | (1 << TXEN0);   // UART RX/TX ON
	UCSR0C |= (1 << UCSZ00) | (1 << UCSZ01); // 8N1

	UBRR0H = (BAUD_PRESCALE >> 8);
	UBRR0L = BAUD_PRESCALE;

	for (i=0; i<64; i++)
	{
		timestring[i]=0;
	}

	set_system_time(0 - UNIX_OFFSET);

	/* Coroutine? */
	while(1)
	{
		while(tick != 0)
		{
			if(tick > 100)
				tick=100;
		};
		localtime_r(&epoch, timeinfo);
		strftime(timestring, 64, "AD %Y/%m/%d [%a] %H:%M:%S", timeinfo);
		fprintf(&mystdout, "%s(%lu, %lu)\r\n", timestring, epoch, epoch + UNIX_OFFSET);

		tick=100;
	}
}

ISR (INT0_vect)
{
	tick--;
	if(tick == 0) epoch++;

	system_tick();
}
